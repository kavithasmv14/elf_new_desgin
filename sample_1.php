<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap 5 Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
  <style>
  body{
  background: #000;
}

.planet{
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
}

.planet__earth {
  width: 27.25rem;
  height: 27.9rem;
  border-radius:50%;
  background-size: cover;
  background-position: 65%;
  box-shadow: -3.65rem -1.15rem 3.025rem 1.1125rem #000 inset, 0 0 0.025rem 0.125rem #000;
  animation: earth-animate 1000s linear alternate infinite;
  transform: rotate(5deg);
  background-image: url("https://i.ibb.co/HY6CcYz/earth-2.jpg");
  position: relative;
}

.planet__earth:active{
  animation-play-state: paused;
}
@keyframes earth-animate{
  100%{
    background-position: 10000%;
  }
}

.planet__satellite{
  position: absolute;
  width: 3rem;
  top: 15%;
  left: 24%;
}
</style>
</head>
<body>

  
<div class="planet">
    <div class="planet__earth" id="earth">
      <img class="planet__satellite" src="https://cdn.pixabay.com/photo/2013/04/01/08/38/satellite-98427_960_720.png" alt="">
    </div>
</div>

<script>
    function earthRotate(){
  const earth = document.getElementById('earth');

  // create a simple instance on our object
  const mc = new Hammer(earth); 

  // add a "PAN" recognizer to it (all directions)
  mc.add( new Hammer.Pan({ direction: Hammer.DIRECTION_ALL, threshold: 0 }) );
  mc.add( new Hammer.Press({ time: 0 }) );

  // tie in the handler that will be called
  mc.on("pan", handleDrag);
  mc.on("press", handlePress);
  mc.on("pressup", handlePressUp);

  // poor choice here, but to keep it simple
  // setting up a few vars to keep track of things.
  // at issue is these values need to be encapsulated
  // in some scope other than global.
  let lastPosX = 0;
  let isDragging = false;
  let posX = -325; //Shift to the desired start position

  function handleDrag(ev) {
    
    // for convience, let's get a reference to our object
    const elem = earth;
    
    // DRAG STARTED
    // here, let's snag the current position
    // and keep track of the fact that we're dragging
    if ( ! isDragging ) {
      startDrag(elem);
    }
    
    // we simply need to determine where the x,y of this
    // object is relative to where it's "last" known position is
    // NOTE: 
    //    deltaX and deltaY are cumulative
    // Thus we need to always calculate 'real x and y' relative
    // to the "lastPosX/Y"
   
    posX = ev.deltaX + lastPosX;
    
    // move our element to that position
    elem.style.backgroundPosition = -(posX / 5) + "%"; //(posX / 5) - interactive rotation speed
    
    // DRAG ENDED
    // this is where we simply forget we are dragging
    if (ev.isFinal) {
      endDrag();
    }
  }

  function handlePress(ev) {
    endDrag();
  }
  function handlePressUp(ev) {
    endDrag();
  }
  function startDrag(elem) {
      isDragging = true;
      lastPosX = posX; //the position from which the interactive rotation will start
  }
  function endDrag() {
      isDragging = false;
  }
}
earthRotate();
</script>


</body>
</html>
