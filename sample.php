<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Elffincorp</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/images_1/ELF_Logo_Gold_Border.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
  <link href="assets/css/roundearth.css" rel="stylesheet">


  <!-- Vendor CSS Files -->
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Arsha - v4.10.0
  * Template URL: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">

      <h1 class="logo me-auto"><a href="index.php"><img src="assets/img/Logo_Blue_Name.png" alt="logo"></a></h1>

      <!-- <h1 class="logo me-auto"><a href="index.html">Elffincorp</a></h1> -->
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo me-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
          <li><a class="nav-link scrollto" href="#about">About</a></li>
          <li><a class="nav-link scrollto" href="#services">Services</a></li>
          <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
          <li><a class="getstarted scrollto" href="#about">Get Started</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav>
      <!-- .navbar -->

    </div>
  </header>
  <!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
            <h1>ELF <span>Fincorp</span></h1>
            <h2>ELF Fin Corp is a client-oriented company,creating new possibilities in the market of leading trading technologies.</h2>
            <div class="d-flex justify-content-center justify-content-lg-start">
                <a href="#about" class="btn-get-started scrollto">Join our Community</a>
            </div>
            </div>
            <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
                <div class="planet">
                    <div class="planet__earth" id="earth"> 
                        <img src="http://localhost/elf_fincorp/assets/img/earth.png" class="img-fluid " alt="earth_image">
                    </div>
                </div>   
            </div>

            <!-- <div class="planet">
                <div class="planet__earth" id="earth">
                    <img class="planet__satellite" src="https://cdn.pixabay.com/photo/2013/04/01/08/38/satellite-98427_960_720.png" alt="">
                </div>
            </div> -->
      </div>
    </div>

  </section>
  <!-- End Hero -->

  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <!-- <div class="section-title">
          <h2>About Us</h2>
        </div> -->
        
        <div class="up">
          <p>About Us</p>
        </div>

        <div class="row content">
          <div class="col-lg-6">
            <img src="assets/img/Plexus_Globe_2.png" alt="about_image" style="width: 90%;height: 90%;"> 
          </div>

          <div class="col-lg-6 pt-4 pt-lg-0">
            <h3>ABOUT :<span>Elf Fin corp is formed by people with decades of experience in financial consulting, tehcnology development, asset managment and marketing. Its a group of like minded people giving the right essence for the the right financial need.<br><br>
              We at ELF Fin Crop is a multi-services platform constantly focus on Delivering /Customizing the products and services facilitated by us regardless of both retail and institutional clients with a wide product range, Tight spreads and fast executions as well as unmatched support to our clients, we have tie ups with top highly regulated liquidity providers in the industry, in simple words our terminal is traders for community.</span></h3>
            <h3>OUR MISSION :<span> mission is to have everything of every kind of market as well as to become the best in Customer support and service and be 100% in TRUST, INTEGRITY, TRANSPARENCY, RELIABILITY and DEDICATION, Improving the quality of our services from time to time is our top priority.</span></h3>
              <!-- <a href="#" class="btn-learn-more">Learn More</a> -->
            <div class="vertical_dotted_line">
              <div class="vertical_spacing"></div>
            </div>
          </div>
        </div>

      </div>
    </section>
    <!-- End About Us Section -->


    <!-- ======= Services Section ======= -->
    <section id="services" class="services section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Services</h2>
          <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
        </div>

        <div class="row justify-content-center text-center">
          <div class="col-sm-2" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <div class="icon-1">
                <div class="icon">
                  <img src="assets/img/Icons/Forex_Brokerage.png" alt="forex_image" style="height:50px;width:50px;"/> 
                </div>
                
                <h4><a href="forex_broker.php">Forex Broker</a></h4>
                <p>ELF Fin Corp offers a wide range of investment and trading services including stock ....</p>
              
                <!-- <div class="btn"> -->
                  <a class="btn-get-started" href="forex_broker.php">Read More</a>
                <!-- </div> -->
              </div>
            </div>
          </div>
          

          <div class="col-sm-2 " data-aos="zoom-in" data-aos-delay="200">
            <div class="icon-box">
              <div class="icon-2">
                <div class="icon">
                  <img src="assets/img/Icons/Crypto_Exchange.png" alt="crypto_image" style="height:50px;width:50px;" /> 
                </div>
                <h4><a href="crypto_exchange.php">Crypto Exchange</a></h4>
                <p>In ELF Fin Corp, users can make instant transactions with Crypto currencies.....</p>
              
                <!-- <div class="btn"> -->
                  <a class="btn-get-started align-middle" href="crypto_exchange.php">Read More</a>
                <!-- </div> -->
              </div>
            </div>
          </div>

          <div class="col-sm-2 " data-aos="zoom-in" data-aos-delay="300">
            <div class="icon-box">
              <div class="icon-3">
                <div class="icon">
                  <img src="assets/img/Icons/Financial_Consulting.png" alt="financial_image" style="height:50px;width:50px;" /> 
                </div>
                <h4><a href="financial_consultion.php">Financial Consulting</a></h4>
                <p>Elf Fin Corp typically advise clients on a range of financial services .....</p>
                
                <!-- <div class="btn"> -->
                  <a class="btn-get-started align-middle" href="financial_consultion.php">Read More</a>
                <!-- </div> -->
              </div>
            </div>
          </div>

          <div class="col-sm-2 " data-aos="zoom-in" data-aos-delay="400">
            <div class="icon-box">
              <div class="icon-4">
                <div class="icon">
                  <img src="assets/img/Icons/Blockchain.png" alt="blockchain_image" style="height:50px;width:50px;"/> 
                </div>
                <h4><a href="blockchain.php">Blockchain Development</a></h4>
                <p>ELF Fin Corp crypto exchange supports the traditional trading platform and....</p>
  
                <!-- <div class="btn"> -->
                  <a class="btn-get-started align-middle" href="blockchain.php">Read More</a>
                <!-- </div> -->
              </div>
            </div>
          </div>

          <div class="col-sm-2 " data-aos="zoom-in" data-aos-delay="400">
            <div class="icon-box">
              <div class="icon-5">
                <div class="icon">
                  <img src="assets/img/Icons/Asset_Management.png" alt="asset_image" style="height:50px;width:50px;"/> 
                </div>
                <h4><a href="asset_management.php">Asset Management</a></h4>
                <p>ELF Fin Corp provides superior baseline asset inventory & tagging services.....</p>
  
                <!-- <div class="btn"> -->
                  <a class="btn-get-started align-middle" href="asset_management.php">Read More</a>
                <!-- </div> -->
              </div>
            </div>
          </div>
        </div>
      </div>

    </section>
    <!-- End Services Section -->


    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Contact</h2>
        </div>

        <div class="row">

          <div class="col-lg-6 d-flex align-items-stretch">
            <div class="info">
              <img src="assets/img/Asset_2@3x_1.png" alt="about_image" style="width: 100%;height: 100%;"> 
            </div>
          </div>

          <div class="col-lg-6 mt-5 mt-lg-0 d-flex align-items-stretch">
            <form action="mail.php" method="post" role="form" class="php-email-form">
              <div class="content-1">
                <h3>Request Free Consultation<h3>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <!-- <label for="name">First Name</label> -->
                  <input type="text" name="fname" class="form-control" id="fname" placeholder="First Name" required>
                </div>
                <div class="form-group col-md-6">
                  <!-- <label for="name">First Name</label> -->
                  <input type="text" name="lname" class="form-control" id="lname"  placeholder="Last Name" required>
                </div>
                <div class="form-group col-md-6">
                  <!-- <label for="name">First Name</label> -->
                  <input type="text" name="phone" class="form-control" id="phone"  placeholder="PhoneNo" required>
                </div>
                <div class="form-group col-md-6">
                  <!-- <label for="name">Your Email</label> -->
                  <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" required>
                </div>
              </div>
              <div class="form-group">
                <!-- <label for="name">Subject</label> -->
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required>
              </div>
              <div class="form-group">
                <!-- <label for="name">Message</label> -->
                <textarea class="form-control" name="message" rows="3" placeholder="Your Message here" required></textarea>
              </div>
              <!-- <div class="my-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
              </div> -->
              <div class="text-center"><button type="submit" name="submit">Send Message</button></div>
            </form>
          </div>

        </div>

      </div>
    </section>
    <!-- End Contact Section -->

    <section id="rs-cta" class="rs-cta">
      <div class="rs-cta style1 bg7 pt-70 pb-70">
        <div class="container">
          <div class="cta-wrap">
            <div class="row align-items-center">
              <div class="col-lg-9 col-md-12 md-pr-0 pr-148 md-pl-15 md-mb-30 md-center">
                <div class="title-wrap">
                  <h2 class="epx-title">Grow Your Business and Build Your Website or Software With us.</h2>
                </div>
              </div>

              <div class="col-lg-3 col-md-12 text-righ">
                  <div class="button-wrapt md-center">
                      <a class="btn-get-started scrollto" href="#about">Get In Touch</a>
                  </div>
              </div>
          </div>
      </div>
    </section>

  </main>
  <!-- End #main -->


 

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-newsletter">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-6">
            <h4>Join Our Newsletter</h4>
            <p>We denounce with righteous and in and dislike men who are so beguiled and demo realized.</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>ELF <span>Fincorp</span></h3>
            <p>
              A108 Adam Street <br>
              New York, NY 535022<br>
              United States <br><br>
              <strong>Phone:</strong> +1 5589 55488 55<br>
              <strong>Email:</strong> support@elffincorp.com<br>
            </p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#hero">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#about">About us</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#services">Services</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#contact">Contact</a></li>
              <!-- <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li> -->
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Services</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="forex_broker.php">Forex Broker</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="crypto_exchange.php">Crypto Exchange</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="asset_management.php">Asset Management</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="financial_consultion.php">Financial Consulting</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="blockchain.php">Blockchain Development</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Social Networks</h4>
            <p>ELF Fin Crop is a multi-services platform constantly focus on Delivering /Customizing the products and services</p>
            <div class="social-links mt-3">
              <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
              <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
              <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="container footer-bottom clearfix">
      <div class="copyright">
        &copy; Copyright <strong><span>ELF Fincorp</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/ -->
        <!-- Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> -->
      </div>
    </div>
  </footer>
  <!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/waypoints/noframework.waypoints.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>